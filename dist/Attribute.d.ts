export declare class Attribute {
    id: string;
    parameters: (number | boolean | string)[];
    constructor(id: string, parameters: (number | boolean | string)[]);
}
