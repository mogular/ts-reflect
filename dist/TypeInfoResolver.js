"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeParameterTypeInfo_1 = require("./typeinfos/TypeParameterTypeInfo");
class TypeInfoResolver {
    constructor() {
        this.dict = {};
        this.register(TypeInfoResolver.TYPE_PARAMETER_NAME, new TypeParameterTypeInfo_1.TypeParameterTypeInfo());
    }
    register(name, info) {
        if (name == null || name != info.name)
            throw new Error("TypeInfo name must be set and info.name must match name!");
        this.dict[name] = info;
    }
    resolve(name) {
        return this.dict[name];
    }
    hasInfo(name) {
        if (!name)
            return false;
        return this.dict[name] != null;
    }
}
TypeInfoResolver.TYPE_PARAMETER_NAME = "_TYPE_PARAMETER";
exports.TypeInfoResolver = TypeInfoResolver;
//# sourceMappingURL=TypeInfoResolver.js.map