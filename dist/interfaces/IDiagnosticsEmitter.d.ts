export declare enum DiagnosticSeverity {
    Warning = 0,
    Error = 1,
    Fatal = 2
}
export interface IDiagnosticsEmitter {
    emit(message: string, path: string, severity?: DiagnosticSeverity, exception?: any): any;
    assert(emitIfFalse: boolean, message: string, path: string, severity?: DiagnosticSeverity): boolean;
    getSupressDiagnosticMessagesEmitter(): IDiagnosticsEmitter;
}
