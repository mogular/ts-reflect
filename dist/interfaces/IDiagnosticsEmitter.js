"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DiagnosticSeverity;
(function (DiagnosticSeverity) {
    DiagnosticSeverity[DiagnosticSeverity["Warning"] = 0] = "Warning";
    DiagnosticSeverity[DiagnosticSeverity["Error"] = 1] = "Error";
    DiagnosticSeverity[DiagnosticSeverity["Fatal"] = 2] = "Fatal";
})(DiagnosticSeverity = exports.DiagnosticSeverity || (exports.DiagnosticSeverity = {}));
//# sourceMappingURL=IDiagnosticsEmitter.js.map