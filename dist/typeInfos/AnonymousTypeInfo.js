"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const PropertyTypeInfo_1 = require("./PropertyTypeInfo");
class AnonymousTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Anonymous);
        this.properties = [];
        this.name = "Anonymous-" + AnonymousTypeInfo.cnt;
        AnonymousTypeInfo.cnt++;
    }
    static fromType(type, resolver) {
        let info = new AnonymousTypeInfo();
        let properties = type.getProperties();
        for (let prop of properties) {
            info.properties.push(PropertyTypeInfo_1.PropertyTypeInfo.fromSymbol(prop, resolver));
        }
        resolver.register(info.name, info);
        return info.name;
    }
}
AnonymousTypeInfo.cnt = 0;
exports.AnonymousTypeInfo = AnonymousTypeInfo;
//# sourceMappingURL=AnonymousTypeInfo.js.map