"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const PropertyTypeInfo_1 = require("./PropertyTypeInfo");
const AttributeFactory_1 = require("../AttributeFactory");
const InterfaceTypeInfo_1 = require("./InterfaceTypeInfo");
class ClassTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Class);
        this.properties = [];
        this.attributes = [];
        this.extends = [];
        this.implements = [];
    }
    static fromClassDeclaration(cd, resolver) {
        let name = cd.getName();
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new ClassTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.text = cd.getText();
        info.attributes = AttributeFactory_1.AttributeFactory.getAttributesFromJsDocs(cd.getJsDocs());
        let properties = cd.getInstanceProperties();
        for (let prop of properties) {
            info.properties.push(PropertyTypeInfo_1.PropertyTypeInfo.fromClassInstancePropertyTypes(prop, resolver));
        }
        let extend = cd.getExtends();
        if (extend) {
            let type = extend.getType();
            let eInfo = ClassTypeInfo.fromType(type, resolver);
        }
        let impl = cd.getImplements();
        if (impl) {
            for (let i of impl) {
                info.implements.push(InterfaceTypeInfo_1.InterfaceTypeInfo.fromType(i.getType(), resolver));
            }
        }
        return info.name;
    }
    static fromType(type, resolver) {
        let cd = type.getSymbol().getValueDeclaration();
        return ClassTypeInfo.fromClassDeclaration(cd, resolver);
    }
}
exports.ClassTypeInfo = ClassTypeInfo;
//# sourceMappingURL=ClassTypeInfo.js.map