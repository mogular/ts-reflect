"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TypeInfoKind;
(function (TypeInfoKind) {
    TypeInfoKind[TypeInfoKind["Primitive"] = 0] = "Primitive";
    TypeInfoKind[TypeInfoKind["Interface"] = 1] = "Interface";
    TypeInfoKind[TypeInfoKind["Class"] = 2] = "Class";
    TypeInfoKind[TypeInfoKind["Enum"] = 3] = "Enum";
    TypeInfoKind[TypeInfoKind["Union"] = 4] = "Union";
    TypeInfoKind[TypeInfoKind["Intersection"] = 5] = "Intersection";
    TypeInfoKind[TypeInfoKind["Array"] = 6] = "Array";
    TypeInfoKind[TypeInfoKind["StringLiteral"] = 7] = "StringLiteral";
    TypeInfoKind[TypeInfoKind["NumberLiteral"] = 8] = "NumberLiteral";
    TypeInfoKind[TypeInfoKind["BooleanLiteral"] = 9] = "BooleanLiteral";
    TypeInfoKind[TypeInfoKind["Anonymous"] = 10] = "Anonymous";
    TypeInfoKind[TypeInfoKind["Tuple"] = 11] = "Tuple";
    TypeInfoKind[TypeInfoKind["Alias"] = 12] = "Alias";
    TypeInfoKind[TypeInfoKind["Generic"] = 13] = "Generic";
    TypeInfoKind[TypeInfoKind["TypeParameter"] = 14] = "TypeParameter";
})(TypeInfoKind = exports.TypeInfoKind || (exports.TypeInfoKind = {}));
class TypeInfo {
    constructor(kind) {
        this.kind = kind;
    }
}
exports.TypeInfo = TypeInfo;
//# sourceMappingURL=TypeInfo.js.map