export declare enum TypeInfoKind {
    Primitive = 0,
    Interface = 1,
    Class = 2,
    Enum = 3,
    Union = 4,
    Intersection = 5,
    Array = 6,
    StringLiteral = 7,
    NumberLiteral = 8,
    BooleanLiteral = 9,
    Anonymous = 10,
    Tuple = 11,
    Alias = 12,
    Generic = 13,
    TypeParameter = 14
}
export declare class TypeInfo {
    private kind;
    name: string;
    constructor(kind: TypeInfoKind);
}
