"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
class PrimitiveTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Primitive);
    }
}
exports.PrimitiveTypeInfo = PrimitiveTypeInfo;
//# sourceMappingURL=PrimitiveTypeInfo.js.map