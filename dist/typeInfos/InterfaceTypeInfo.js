"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const PropertyTypeInfo_1 = require("./PropertyTypeInfo");
const AttributeFactory_1 = require("../AttributeFactory");
class InterfaceTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Interface);
        this.properties = [];
        this.attributes = [];
        this.extends = [];
    }
    static fromInterfaceDeclaration(id, resolver) {
        let name = id.getName();
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new InterfaceTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.text = id.getText();
        info.attributes = AttributeFactory_1.AttributeFactory.getAttributesFromJsDocs(id.getJsDocs());
        let properties = id.getProperties();
        for (let prop of properties) {
            info.properties.push(PropertyTypeInfo_1.PropertyTypeInfo.fromPropertySignature(prop, resolver));
        }
        let extend = id.getExtends();
        if (extend) {
            for (let e of extend) {
                info.extends.push(InterfaceTypeInfo.fromType(e.getType(), resolver));
            }
        }
        return name;
    }
    static fromType(type, resolver) {
        let id = type.getSymbol().getDeclarations()[0];
        return InterfaceTypeInfo.fromInterfaceDeclaration(id, resolver);
    }
}
exports.InterfaceTypeInfo = InterfaceTypeInfo;
//# sourceMappingURL=InterfaceTypeInfo.js.map