"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const Factory_1 = require("./Factory");
class UnionTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Union);
        this.unionTypes = [];
    }
    static fromType(type, resolver) {
        let name;
        let unTypes = type.getUnionTypes();
        let symbol = type.getSymbol();
        if (symbol)
            name = symbol.getName();
        else
            name = unTypes.map(t => Factory_1.Factory.fromType(t, resolver)).join(" | ");
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new UnionTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.unionTypes = unTypes.map(u => Factory_1.Factory.fromType(u, resolver));
        return name;
    }
}
exports.UnionTypeInfo = UnionTypeInfo;
//# sourceMappingURL=UnionTypeInfo.js.map