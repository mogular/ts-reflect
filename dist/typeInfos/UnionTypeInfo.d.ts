import { TypeInfo } from "./TypeInfo";
import { Type } from 'ts-simple-ast';
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class UnionTypeInfo extends TypeInfo {
    unionTypes: string[];
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
