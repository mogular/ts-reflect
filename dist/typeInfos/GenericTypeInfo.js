"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const main_1 = require("../main");
const Factory_1 = require("./Factory");
class GenericTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Generic);
        this.attributes = [];
        this.typeArguments = [];
    }
    static fromType(type, resolver) {
        let symbol = type.getSymbol();
        let args = type.getTypeArguments().map(a => a.getText());
        let name = symbol.getName() + `<${args.join(",")}>`;
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new GenericTypeInfo();
        info.name = name;
        info.typeArguments = type.getTypeArguments().map(t => {
            let text = t.getText();
            let upper = text.toUpperCase();
            if (upper == "ANY" || upper == "STRING" || upper == "NUMBER" || upper == "BOOLEAN" || upper == "OBJECT") //This is pretty much dirty but it works...
                return upper;
            return text;
        });
        resolver.register(name, info);
        info.targetType = Factory_1.Factory.fromType(type.getTargetType(), resolver);
        info.text = type.getText();
        info.attributes = main_1.AttributeFactory.getAttributesFromJsDocs(type.compilerType.jsDoc);
        return info.name;
    }
}
exports.GenericTypeInfo = GenericTypeInfo;
//# sourceMappingURL=GenericTypeInfo.js.map