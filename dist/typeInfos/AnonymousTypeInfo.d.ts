import { TypeInfo } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class AnonymousTypeInfo extends TypeInfo {
    static cnt: number;
    properties: PropertyTypeInfo[];
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
