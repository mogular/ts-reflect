import { TypeInfo } from "./TypeInfo";
import { Type } from 'ts-simple-ast';
import { TypeInfoResolver } from "../TypeInfoResolver";
export declare class ArrayTypeInfo extends TypeInfo {
    arrayType: string;
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
