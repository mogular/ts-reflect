"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const Factory_1 = require("./Factory");
class ArrayTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Array);
    }
    static fromType(type, resolver) {
        let arType = type.getArrayType();
        let arTypeName = Factory_1.Factory.fromType(arType, resolver);
        let name = arTypeName + "[]";
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new ArrayTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.arrayType = arTypeName;
        return name;
    }
}
exports.ArrayTypeInfo = ArrayTypeInfo;
//# sourceMappingURL=ArrayTypeInfo.js.map