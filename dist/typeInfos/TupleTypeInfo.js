"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const Factory_1 = require("./Factory");
class TupleTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Tuple);
        this.tupleTypes = [];
    }
    static fromType(type, resolver) {
        let name;
        let tupleTypes = type.getTupleElements();
        let symbol = type.getSymbol();
        let tupleNames = tupleTypes.map(t => Factory_1.Factory.fromType(t, resolver));
        if (symbol)
            name = symbol.getName();
        else
            name = "[" + tupleNames.join(",") + "]";
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new TupleTypeInfo();
        info.name = name;
        info.tupleTypes = tupleNames;
        resolver.register(name, info);
        return info.name;
    }
}
TupleTypeInfo.cnt = 0;
exports.TupleTypeInfo = TupleTypeInfo;
//# sourceMappingURL=TupleTypeInfo.js.map