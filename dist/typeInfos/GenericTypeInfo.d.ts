import { TypeInfo } from "./TypeInfo";
import { Attribute } from '../Attribute';
import { Type } from 'ts-simple-ast';
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class GenericTypeInfo extends TypeInfo {
    attributes: Attribute[];
    text: string;
    targetType: string;
    typeArguments: string[];
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
