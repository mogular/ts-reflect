import { TypeInfo } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { ClassDeclaration, Type } from "ts-simple-ast";
import { TypeInfoResolver } from "../TypeInfoResolver";
export declare class ClassTypeInfo extends TypeInfo {
    properties: PropertyTypeInfo[];
    attributes: Attribute[];
    text: string;
    extends: string[];
    implements: string[];
    constructor();
    static fromClassDeclaration(cd: ClassDeclaration, resolver: TypeInfoResolver): string;
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
