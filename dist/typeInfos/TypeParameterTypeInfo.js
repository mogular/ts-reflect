"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const TypeInfoResolver_1 = require("../TypeInfoResolver");
class TypeParameterTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.TypeParameter);
        this.name = TypeInfoResolver_1.TypeInfoResolver.TYPE_PARAMETER_NAME;
    }
}
exports.TypeParameterTypeInfo = TypeParameterTypeInfo;
//# sourceMappingURL=TypeParameterTypeInfo.js.map