"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const AttributeFactory_1 = require("../AttributeFactory");
class EnumTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Enum);
        this.members = [];
        this.attributes = [];
    }
    static fromDeclaration(ed, resolver) {
        let name = ed.getName();
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new EnumTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.members = ed.getMembers().map(m => {
            return { key: m.getName(), value: m.getValue() };
        });
        info.attributes = AttributeFactory_1.AttributeFactory.getAttributesFromJsDocs(ed.getJsDocs());
        return name;
    }
    static fromType(type, resolver) {
        return EnumTypeInfo.fromDeclaration(type.getSymbol().getValueDeclaration(), resolver);
    }
}
exports.EnumTypeInfo = EnumTypeInfo;
//# sourceMappingURL=EnumTypeInfo.js.map