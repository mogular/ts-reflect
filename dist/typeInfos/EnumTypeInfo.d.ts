import { TypeInfo } from "./TypeInfo";
import { Attribute } from '../Attribute';
import { EnumDeclaration, Type } from 'ts-simple-ast';
import { TypeInfoResolver } from '../TypeInfoResolver';
export interface EnumMember {
    value: number | string;
    key: string;
}
export declare class EnumTypeInfo extends TypeInfo {
    members: EnumMember[];
    attributes: Attribute[];
    constructor();
    static fromDeclaration(ed: EnumDeclaration, resolver: TypeInfoResolver): string;
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
