import { TypeInfo } from "./TypeInfo";
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from "../main";
export declare class TupleTypeInfo extends TypeInfo {
    value: string;
    static cnt: number;
    tupleTypes: string[];
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
