import { TypeInfo } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { Type, InterfaceDeclaration } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class InterfaceTypeInfo extends TypeInfo {
    properties: PropertyTypeInfo[];
    attributes: Attribute[];
    text: string;
    extends: string[];
    constructor();
    static fromInterfaceDeclaration(id: InterfaceDeclaration, resolver: TypeInfoResolver): string;
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
