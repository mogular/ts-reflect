import { IDiagnosticsEmitter, DiagnosticSeverity } from './interfaces/IDiagnosticsEmitter';
export declare class DummyDiagnosticsEmitter implements IDiagnosticsEmitter {
    private suppressed;
    constructor(suppressed?: boolean);
    emit(message: string, path: string, severity?: DiagnosticSeverity, exception?: any): void;
    assert(emitIfFalse: boolean, message: string, path: string, severity?: DiagnosticSeverity): boolean;
    getSupressDiagnosticMessagesEmitter(): IDiagnosticsEmitter;
}
