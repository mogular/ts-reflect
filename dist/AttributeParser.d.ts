export declare enum NodeKind {
    Root = 0,
    AttributeDeclaration = 1,
    Attribute = 2,
    StringParameter = 3,
    NumberParameter = 4
}
export declare class AttributeParser {
    private state;
    private buffer;
    private whitespaceRegex;
    private symbolRegex;
    private numberRegex;
    private position;
    private rootNode;
    private currentNode;
    parse(text: string): Node;
    private processToken;
}
export declare class Node {
    kind: NodeKind;
    value: string;
    start: number;
    end: number;
    children: Node[];
    parent: Node;
    kindString: string;
    constructor(kind: NodeKind, parent?: Node, value?: string, start?: number, end?: number);
    addChild(kind: NodeKind, value?: string, start?: number, end?: number): Node;
}
