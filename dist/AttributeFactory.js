"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AttributeParser_1 = require("./AttributeParser");
const Attribute_1 = require("./Attribute");
class AttributeFactory {
    // public static attributeList: {id: string, ctor: Constructor<Attribute>}[] = [];
    // public static registerAttribute(attribute: Attribute)
    // {
    //     this.attributeList.push({id: attribute.id, ctor: (<any> attribute).constructor});
    // }
    static createAttributes(node) {
        let res = [];
        for (let attributeDeclarationNode of node.children) {
            for (let attributeNode of attributeDeclarationNode.children) {
                let params = attributeNode.children.map(c => c.value);
                res.push(new Attribute_1.Attribute(attributeNode.value, params));
            }
        }
        return res;
    }
    static getAttributesFromJsDocs(jsdocs) {
        if (jsdocs && jsdocs.length > 0) {
            let doc = jsdocs.map(c => c.getComment()).join(" ");
            let parser = new AttributeParser_1.AttributeParser();
            let node = parser.parse(doc);
            return AttributeFactory.createAttributes(node);
        }
        return [];
    }
}
exports.AttributeFactory = AttributeFactory;
//# sourceMappingURL=AttributeFactory.js.map