export declare class Util {
    static iterableToArray<T>(i: Iterable<T>): T[];
    static getParentJsonPath(path: string): string;
}
