"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts_simple_ast_1 = require("ts-simple-ast");
const path = require("path");
const ClassTypeInfo_1 = require("./typeinfos/ClassTypeInfo");
const InterfaceTypeInfo_1 = require("./typeinfos/InterfaceTypeInfo");
const TypeInfoResolver_1 = require("./TypeInfoResolver");
class Reflection {
    static init() {
        this.project = new ts_simple_ast_1.default();
    }
    static createFile(path, text) {
        try {
            return this.project.createSourceFile(path, text);
        }
        catch (error) {
            console.log(error);
            return null;
        }
    }
    static clear() {
        Reflection.project = null;
    }
    static addFile(path) {
        try {
            return this.project.addExistingSourceFile(path);
        }
        catch (error) {
            console.log(error);
            return null;
        }
    }
    static addDir(dirPath) {
        try {
            this.project.addExistingSourceFiles(path.join(dirPath, "*.ts"));
        }
        catch (error) {
            console.log(error);
        }
    }
    static searchType(name) {
        let upper = name.toUpperCase();
        let c = null;
        for (let file of this.project.getSourceFiles()) {
            if (upper == "ANY")
                return this.project.getTypeChecker().compilerObject.typeParameterToDeclaration;
            c = file.getClass(name).getType();
            if (c)
                break;
            c = file.getInterface(name).getType();
            if (c)
                break;
        }
        return c;
    }
    static getClassTypeInfo(name, resolver = null) {
        let c = null;
        for (let file of this.project.getSourceFiles()) {
            c = file.getClass(name);
            if (c)
                break;
        }
        if (!c)
            return null;
        if (!resolver)
            resolver = new TypeInfoResolver_1.TypeInfoResolver();
        let n = ClassTypeInfo_1.ClassTypeInfo.fromClassDeclaration(c, resolver);
        return { info: resolver.resolve(n), resolver: resolver };
    }
    static getInterfaceTypeInfo(name, resolver) {
        let c = null;
        for (let file of this.project.getSourceFiles()) {
            c = file.getInterface(name);
            if (c)
                break;
        }
        if (!c)
            return null;
        if (!resolver)
            resolver = new TypeInfoResolver_1.TypeInfoResolver();
        let n = InterfaceTypeInfo_1.InterfaceTypeInfo.fromInterfaceDeclaration(c, resolver);
        return { info: resolver.resolve(n), resolver: resolver };
    }
    static getDescendantDeclarations(name) {
        let res = [];
        for (let file of this.project.getSourceFiles().filter(f => f.getExtension() == ".ts")) {
            try {
                let matchingDeclarations = file.getExportedDeclarations()
                    .filter(d => d.getKind() == ts_simple_ast_1.SyntaxKind.InterfaceDeclaration || d.getKind() == ts_simple_ast_1.SyntaxKind.ClassDeclaration)
                    .filter(d => {
                    let declaration = d;
                    let ex = declaration.getExtends();
                    if (ex) {
                        if (Array.isArray(ex)) {
                            return ex.map(e => e.getType().getSymbol().getName()).indexOf(name) >= 0;
                        }
                        else {
                            return ex.getType().getSymbol().getName() == name;
                        }
                    }
                });
                res = res.concat(matchingDeclarations);
            }
            catch (error) {
                console.log(`Could not get descendant types, error: ${error}`);
                return null;
            }
        }
        return res;
    }
}
exports.Reflection = Reflection;
//# sourceMappingURL=Reflection.js.map