"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PrimitiveTypeInfo_1 = require("./PrimitiveTypeInfo");
const ClassTypeInfo_1 = require("./ClassTypeInfo");
const InterfaceTypeInfo_1 = require("./InterfaceTypeInfo");
const AnonymousTypeInfo_1 = require("./AnonymousTypeInfo");
const ArrayTypeInfo_1 = require("./ArrayTypeInfo");
const EnumTypeInfo_1 = require("./EnumTypeInfo");
const UnionTypeInfo_1 = require("./UnionTypeInfo");
const IntersectionTypeInfo_1 = require("./IntersectionTypeInfo");
const NumberLiteralTypeInfo_1 = require("./NumberLiteralTypeInfo");
const BooleanLiteralTypeInfo_1 = require("./BooleanLiteralTypeInfo");
const StringLiteralTypeInfo_1 = require("./StringLiteralTypeInfo");
const TypeInfoResolver_1 = require("../TypeInfoResolver");
const TupleTypeInfo_1 = require("./TupleTypeInfo");
const AliasTypeInfo_1 = require("./AliasTypeInfo");
const GenericTypeInfo_1 = require("./GenericTypeInfo");
class Factory {
    static fromType(type, resolver) {
        let symbol = type.getSymbol();
        let name;
        if (symbol) {
            name = symbol.getName();
            if (resolver.hasInfo(name) == true)
                return name;
        }
        let text = type.getText();
        let upperText = text.toUpperCase();
        if (type.isString() === true || type.isNumber() === true || type.isBoolean() === true || type.isUndefined() === true) {
            name = upperText;
            if (resolver.hasInfo(name) == true)
                return name;
            let info = new PrimitiveTypeInfo_1.PrimitiveTypeInfo();
            info.name = upperText;
            resolver.register(info.name, info);
            name = info.name;
        }
        else if (upperText == "ANY" || upperText == "OBJECT") {
            name = upperText;
            if (resolver.hasInfo(name) == true)
                return name;
            let info = new PrimitiveTypeInfo_1.PrimitiveTypeInfo();
            info.name = upperText;
            resolver.register(info.name, info);
            name = info.name;
        }
        else if (type.isArray() === true) {
            name = ArrayTypeInfo_1.ArrayTypeInfo.fromType(type, resolver);
        }
        else if (type.isTuple() === true) {
            name = TupleTypeInfo_1.TupleTypeInfo.fromType(type, resolver);
        }
        else if (type.isEnum() === true || type.isEnumLiteral()) {
            name = EnumTypeInfo_1.EnumTypeInfo.fromType(type, resolver);
        }
        else if (type.isUnion() === true) {
            name = UnionTypeInfo_1.UnionTypeInfo.fromType(type, resolver);
        }
        else if (type.isIntersection() === true) {
            name = IntersectionTypeInfo_1.IntersectionTypeInfo.fromType(type, resolver);
        }
        else if (type.isAnonymous() === true) {
            name = AnonymousTypeInfo_1.AnonymousTypeInfo.fromType(type, resolver);
        }
        else if (type.isClass() === true) {
            name = ClassTypeInfo_1.ClassTypeInfo.fromType(type, resolver);
        }
        else if (type.isInterface() === true) {
            name = InterfaceTypeInfo_1.InterfaceTypeInfo.fromType(type, resolver);
        }
        else if (type.isNumberLiteral() === true) {
            name = NumberLiteralTypeInfo_1.NumberLiteralTypeInfo.fromType(type, resolver);
        }
        else if (type.isBooleanLiteral() === true) {
            name = BooleanLiteralTypeInfo_1.BooleanLiteralTypeInfo.fromType(type, resolver);
        }
        else if (type.isStringLiteral() === true) {
            name = StringLiteralTypeInfo_1.StringLiteralTypeInfo.fromType(type, resolver);
        }
        else if (type.getTypeArguments().length > 0) {
            name = GenericTypeInfo_1.GenericTypeInfo.fromType(type, resolver);
        }
        else if (type.isTypeParameter()) {
            name = resolver.resolve(TypeInfoResolver_1.TypeInfoResolver.TYPE_PARAMETER_NAME).name;
        }
        else {
            console.log(`Warning: No factory case applies to type ${name}, returning 'any' AliasType!`);
            let t = new AliasTypeInfo_1.AliasTypeInfo(name, "ANY");
            resolver.register(name, t);
        }
        return name;
    }
}
exports.Factory = Factory;
//# sourceMappingURL=Factory.js.map