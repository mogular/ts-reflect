"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
class AliasTypeInfo extends TypeInfo_1.TypeInfo {
    constructor(aliasName, typeName) {
        super(TypeInfo_1.TypeInfoKind.Alias);
        this.name = aliasName;
        this.typeName = typeName;
    }
}
exports.AliasTypeInfo = AliasTypeInfo;
//# sourceMappingURL=AliasTypeInfo.js.map