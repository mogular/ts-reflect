"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AttributeFactory_1 = require("../AttributeFactory");
const Factory_1 = require("./Factory");
class PropertyTypeInfo {
    constructor() {
        this.attributes = [];
        this.isRequired = true;
    }
    static fromClassInstancePropertyTypes(p, resolver) {
        let prop = new PropertyTypeInfo();
        prop.name = p.getName();
        if (p.compilerNode.questionToken) {
            prop.isRequired = false;
        }
        prop.attributes = AttributeFactory_1.AttributeFactory.getAttributesFromJsDocs(p.compilerNode.jsDoc);
        prop.type = Factory_1.Factory.fromType(p.getType(), resolver);
        return prop;
    }
    static fromPropertySignature(p, resolver) {
        let prop = new PropertyTypeInfo();
        prop.name = p.getName();
        if (p.compilerNode.questionToken) {
            prop.isRequired = false;
        }
        prop.attributes = AttributeFactory_1.AttributeFactory.getAttributesFromJsDocs(p.getJsDocs());
        prop.type = Factory_1.Factory.fromType(p.getType(), resolver);
        return prop;
    }
    static fromSymbol(s, resolver) {
        return PropertyTypeInfo.fromPropertySignature(s.getValueDeclaration(), resolver);
    }
}
exports.PropertyTypeInfo = PropertyTypeInfo;
//# sourceMappingURL=PropertyTypeInfo.js.map