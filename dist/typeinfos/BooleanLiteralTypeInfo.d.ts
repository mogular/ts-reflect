import { TypeInfo } from "./TypeInfo";
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class BooleanLiteralTypeInfo extends TypeInfo {
    value: boolean;
    static cnt: number;
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
