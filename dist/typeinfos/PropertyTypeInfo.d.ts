import { Attribute } from '../Attribute';
import { ClassInstancePropertyTypes, PropertySignature, Symbol } from 'ts-simple-ast';
import { TypeInfoResolver } from "../TypeInfoResolver";
export declare class PropertyTypeInfo {
    name: string;
    type: string;
    attributes: Attribute[];
    isRequired: boolean;
    static fromClassInstancePropertyTypes(p: ClassInstancePropertyTypes, resolver: TypeInfoResolver): PropertyTypeInfo;
    static fromPropertySignature(p: PropertySignature, resolver: TypeInfoResolver): PropertyTypeInfo;
    static fromSymbol(s: Symbol, resolver: TypeInfoResolver): PropertyTypeInfo;
}
