"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
class NumberLiteralTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.NumberLiteral);
        this.name = "NumberLiteral-" + NumberLiteralTypeInfo.cnt;
        NumberLiteralTypeInfo.cnt++;
    }
    static fromType(type, resolver) {
        let info = new NumberLiteralTypeInfo();
        info.value = Number.parseFloat(type.getText().trim());
        resolver.register(info.name, info);
        return info.name;
    }
}
NumberLiteralTypeInfo.cnt = 0;
exports.NumberLiteralTypeInfo = NumberLiteralTypeInfo;
//# sourceMappingURL=NumberLiteralTypeInfo.js.map