"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
class StringLiteralTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.StringLiteral);
        this.name = "StringLiteral-" + StringLiteralTypeInfo.cnt;
        StringLiteralTypeInfo.cnt++;
    }
    static fromType(type, resolver) {
        let info = new StringLiteralTypeInfo();
        info.value = type.getText();
        if (info.value.startsWith("\"") || info.value.startsWith("'"))
            info.value = info.value.substr(1);
        if (info.value.endsWith("\"") || info.value.endsWith("'"))
            info.value = info.value.substr(0, info.value.length - 1);
        resolver.register(info.name, info);
        return info.name;
    }
}
StringLiteralTypeInfo.cnt = 0;
exports.StringLiteralTypeInfo = StringLiteralTypeInfo;
//# sourceMappingURL=StringLiteralTypeInfo.js.map