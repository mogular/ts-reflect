import { TypeInfo } from "./TypeInfo";
import { Type } from 'ts-simple-ast';
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class IntersectionTypeInfo extends TypeInfo {
    intersectionTypes: string[];
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
