import { TypeInfo } from "./TypeInfo";
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class NumberLiteralTypeInfo extends TypeInfo {
    value: number;
    static cnt: number;
    constructor();
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
