"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
class BooleanLiteralTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.BooleanLiteral);
        this.name = "BooleanLiteral-" + BooleanLiteralTypeInfo.cnt;
        BooleanLiteralTypeInfo.cnt++;
    }
    static fromType(type, resolver) {
        let info = new BooleanLiteralTypeInfo();
        info.value = type.getText() == "true";
        resolver.register(info.name, info);
        return info.name;
    }
}
BooleanLiteralTypeInfo.cnt = 0;
exports.BooleanLiteralTypeInfo = BooleanLiteralTypeInfo;
//# sourceMappingURL=BooleanLiteralTypeInfo.js.map