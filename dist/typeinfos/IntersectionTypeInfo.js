"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TypeInfo_1 = require("./TypeInfo");
const Factory_1 = require("./Factory");
class IntersectionTypeInfo extends TypeInfo_1.TypeInfo {
    constructor() {
        super(TypeInfo_1.TypeInfoKind.Interface);
        this.intersectionTypes = [];
    }
    static fromType(type, resolver) {
        let intTypes = type.getIntersectionTypes();
        let symbol = type.getSymbol();
        let name;
        if (symbol)
            name = symbol.getName();
        else
            name = intTypes.map(t => Factory_1.Factory.fromType(t, resolver)).join(" | ");
        if (resolver.hasInfo(name) == true)
            return name;
        let info = new IntersectionTypeInfo;
        info.name = name;
        resolver.register(name, info);
        info.intersectionTypes = type.getIntersectionTypes().map(t => {
            return Factory_1.Factory.fromType(t, resolver);
        });
        return name;
    }
}
exports.IntersectionTypeInfo = IntersectionTypeInfo;
//# sourceMappingURL=IntersectionTypeInfo.js.map