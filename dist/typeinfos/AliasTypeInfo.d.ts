import { TypeInfo } from "./TypeInfo";
export declare class AliasTypeInfo extends TypeInfo {
    typeName: string;
    constructor(aliasName: string, typeName: string);
}
