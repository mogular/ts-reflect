import { Type } from 'ts-simple-ast';
import { TypeInfoResolver } from '../TypeInfoResolver';
export declare class Factory {
    static fromType(type: Type, resolver: TypeInfoResolver): string;
}
