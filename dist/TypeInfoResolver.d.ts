import { TypeInfo } from './typeinfos/TypeInfo';
export declare class TypeInfoResolver {
    static TYPE_PARAMETER_NAME: string;
    dict: any;
    constructor();
    register(name: string, info: TypeInfo): void;
    resolve(name: string): TypeInfo;
    hasInfo(name: string): boolean;
}
