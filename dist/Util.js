"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Util {
    static iterableToArray(i) {
        let res = [];
        for (let elem of i) {
            res.push(elem);
        }
        return res;
    }
    static getParentJsonPath(path) {
        let ar = path.split(".");
        ar.splice(-1, 1);
        return ar.join(".");
    }
}
exports.Util = Util;
//# sourceMappingURL=Util.js.map