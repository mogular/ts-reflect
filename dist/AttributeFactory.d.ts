import { Node } from './AttributeParser';
import { JSDoc } from 'ts-simple-ast';
import { Attribute } from './Attribute';
export declare class AttributeFactory {
    static createAttributes(node: Node): Attribute[];
    static getAttributesFromJsDocs(jsdocs: JSDoc[]): Attribute[];
}
