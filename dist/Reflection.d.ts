import Project, { SourceFile, ClassDeclaration, Type, ts } from "ts-simple-ast";
import { TypeInfo } from './typeinfos/TypeInfo';
import { InterfaceDeclaration } from 'ts-simple-ast';
import { TypeInfoResolver } from './TypeInfoResolver';
export declare class Reflection {
    static project: Project;
    static init(): void;
    static createFile(path: string, text: string): SourceFile;
    static clear(): void;
    static addFile(path: string): SourceFile;
    static addDir(dirPath: string): void;
    static searchType(name: string): Type<ts.Type> | ((parameter: ts.TypeParameter, enclosingDeclaration?: ts.Node, flags?: ts.NodeBuilderFlags) => ts.TypeParameterDeclaration);
    static getClassTypeInfo(name: string, resolver?: TypeInfoResolver): {
        info: TypeInfo;
        resolver: TypeInfoResolver;
    };
    static getInterfaceTypeInfo(name: string, resolver: TypeInfoResolver): {
        info: TypeInfo;
        resolver: TypeInfoResolver;
    };
    static getDescendantDeclarations(name: string): (InterfaceDeclaration | ClassDeclaration)[];
}
