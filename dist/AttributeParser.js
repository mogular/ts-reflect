"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AttributeParserState;
(function (AttributeParserState) {
    AttributeParserState[AttributeParserState["Idle"] = 0] = "Idle";
    AttributeParserState[AttributeParserState["ReadingAttributes"] = 1] = "ReadingAttributes";
    AttributeParserState[AttributeParserState["ReadingAttributeId"] = 2] = "ReadingAttributeId";
    AttributeParserState[AttributeParserState["ReadingAttributeParameters"] = 3] = "ReadingAttributeParameters";
    AttributeParserState[AttributeParserState["ReadingStringAttributeParameter"] = 4] = "ReadingStringAttributeParameter";
    AttributeParserState[AttributeParserState["ReadingNumberAttributeParameter"] = 5] = "ReadingNumberAttributeParameter";
    AttributeParserState[AttributeParserState["ReadingEscapeCharacter"] = 6] = "ReadingEscapeCharacter";
})(AttributeParserState || (AttributeParserState = {}));
var NodeKind;
(function (NodeKind) {
    NodeKind[NodeKind["Root"] = 0] = "Root";
    NodeKind[NodeKind["AttributeDeclaration"] = 1] = "AttributeDeclaration";
    NodeKind[NodeKind["Attribute"] = 2] = "Attribute";
    NodeKind[NodeKind["StringParameter"] = 3] = "StringParameter";
    NodeKind[NodeKind["NumberParameter"] = 4] = "NumberParameter";
})(NodeKind = exports.NodeKind || (exports.NodeKind = {}));
class AttributeParser {
    constructor() {
        this.state = AttributeParserState.Idle;
        this.buffer = "";
        this.whitespaceRegex = /\s/;
        this.symbolRegex = /[\w_]/;
        this.numberRegex = /[0-9]/;
        this.position = 0;
        this.rootNode = new Node(NodeKind.Root);
        this.currentNode = null;
    }
    parse(text) {
        let state = AttributeParserState.Idle;
        for (let i = 0; i < text.length; i++) {
            this.processToken(text[i]);
            this.position++;
        }
        let ret = this.rootNode;
        this.rootNode = new Node(NodeKind.Root);
        this.currentNode = this.rootNode;
        this.position = 0;
        return ret;
    }
    processToken(token) {
        switch (this.state) {
            case AttributeParserState.Idle:
                {
                    if (token == "[") {
                        this.currentNode = this.rootNode.addChild(NodeKind.AttributeDeclaration);
                        this.currentNode.start = this.position;
                        this.state = AttributeParserState.ReadingAttributes;
                    }
                    break;
                }
            case AttributeParserState.ReadingAttributes:
                {
                    if (token.match(this.symbolRegex)) {
                        this.buffer += token;
                        let node = this.currentNode.addChild(NodeKind.Attribute);
                        node.start = this.position;
                        this.currentNode = node;
                        this.state = AttributeParserState.ReadingAttributeId;
                    }
                    else if (token == "]") {
                        this.currentNode.end = this.position;
                        this.state = AttributeParserState.Idle;
                        this.currentNode = this.rootNode;
                    }
                    else if (token == ",") {
                    }
                    else if (token.match(this.whitespaceRegex)) {
                    }
                    else {
                        console.log(`Unexpected token ${token} at position ${this.position}`);
                    }
                    break;
                }
            case AttributeParserState.ReadingAttributeId:
                {
                    if (token.match(this.symbolRegex)) {
                        this.buffer += token;
                    }
                    else if (token == "(") {
                        this.currentNode.value = this.buffer;
                        this.buffer = "";
                        this.currentNode.end = this.position;
                        this.state = AttributeParserState.ReadingAttributeParameters;
                    }
                    else if (token == ",") {
                        this.currentNode.value = this.buffer;
                        this.buffer = "";
                        this.currentNode.end = this.position;
                        this.currentNode = this.currentNode.parent; //set current node to NodeKind.AttributeDeclaration
                        this.state = AttributeParserState.ReadingAttributes;
                    }
                    else if (token.match(this.whitespaceRegex)) {
                        this.currentNode.value = this.buffer;
                        this.buffer = "";
                        this.currentNode.end = this.position;
                        this.currentNode = this.currentNode.parent;
                        this.state = AttributeParserState.ReadingAttributes;
                    }
                    else if (token == "]") {
                        this.currentNode.value = this.buffer;
                        this.buffer = "";
                        this.currentNode.end = this.position;
                        this.state = AttributeParserState.Idle;
                        this.currentNode = this.rootNode;
                    }
                    break;
                }
            case AttributeParserState.ReadingAttributeParameters:
                {
                    if (token.match(this.numberRegex)) {
                        this.buffer += token;
                        let node = this.currentNode.addChild(NodeKind.NumberParameter);
                        node.start = this.position;
                        this.currentNode = node;
                        this.state = AttributeParserState.ReadingNumberAttributeParameter;
                    }
                    if (token == "\"") {
                        let node = this.currentNode.addChild(NodeKind.StringParameter);
                        node.start = this.position;
                        this.currentNode = node;
                        this.state = AttributeParserState.ReadingStringAttributeParameter;
                    }
                    else if (token == ")") {
                        this.currentNode.end = this.position;
                        this.currentNode = this.currentNode.parent;
                        this.state = AttributeParserState.ReadingAttributes;
                    }
                    else if (token == ",") {
                    }
                    else if (token.match(this.whitespaceRegex)) {
                    }
                    else {
                        console.log(`Unexpected token ${token} at position ${this.position}`);
                    }
                    break;
                }
            case AttributeParserState.ReadingNumberAttributeParameter:
                {
                    if (token.match(this.numberRegex)) {
                        this.buffer += token;
                    }
                    else if (token == ",") {
                        this.currentNode.value = this.buffer;
                        this.buffer = "";
                        this.currentNode.end = this.position;
                        this.currentNode = this.currentNode.parent;
                        this.state = AttributeParserState.ReadingAttributeParameters;
                    }
                    else if (token.match(this.whitespaceRegex)) {
                        this.currentNode.value = this.buffer;
                        this.buffer = "";
                        this.currentNode.end = this.position;
                        this.currentNode = this.currentNode.parent;
                        this.state = AttributeParserState.ReadingAttributeParameters;
                    }
                    break;
                }
            case AttributeParserState.ReadingStringAttributeParameter:
                {
                    if (token == "\\") {
                        this.state = AttributeParserState.ReadingEscapeCharacter;
                    }
                    else if (token == "\"") {
                        this.currentNode.value = this.buffer;
                        this.buffer = "";
                        this.currentNode.end = this.position;
                        this.currentNode = this.currentNode.parent;
                        this.state = AttributeParserState.ReadingAttributeParameters;
                    }
                    else {
                        this.buffer += token;
                    }
                    break;
                }
            case AttributeParserState.ReadingEscapeCharacter:
                {
                    if (token == "a") {
                        this.buffer += "\a";
                    }
                    else if (token == "b") {
                        this.buffer += "\b";
                    }
                    else if (token == "e") {
                        this.buffer += "\e";
                    }
                    else if (token == "E") {
                        this.buffer += "\E";
                    }
                    else if (token == "f") {
                        this.buffer += "\f";
                    }
                    else if (token == "n") {
                        this.buffer += "\n";
                    }
                    else if (token == "t") {
                        this.buffer += "\t";
                    }
                    else if (token == "v") {
                        this.buffer += "\v";
                    }
                    else if (token == "\"") {
                        this.buffer += "\"";
                    }
                    else {
                        console.log(`Unrecognized escape sequence \\${token}`);
                    }
                    this.state = AttributeParserState.ReadingStringAttributeParameter;
                    break;
                }
        }
    }
}
exports.AttributeParser = AttributeParser;
class Node {
    constructor(kind, parent = null, value = null, start = null, end = null) {
        this.kind = kind;
        this.start = null;
        this.end = null;
        this.children = [];
        this.parent = parent;
        this.value = value;
        this.start = start;
        this.end = end;
        this.kindString = NodeKind[kind];
    }
    addChild(kind, value = null, start = null, end = null) {
        let node = new Node(kind, this);
        this.children.push(node);
        return node;
    }
}
exports.Node = Node;
//# sourceMappingURL=AttributeParser.js.map