"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DummyDiagnosticsEmitter {
    constructor(suppressed = false) {
        this.suppressed = suppressed;
    }
    emit(message, path, severity, exception) {
        if (!this.suppressed) {
            console.log(`"${message}" at "${path}"`);
        }
    }
    assert(emitIfFalse, message, path, severity) {
        if (!emitIfFalse) {
            this.emit(message, path, severity);
        }
        return !emitIfFalse;
    }
    getSupressDiagnosticMessagesEmitter() {
        return new DummyDiagnosticsEmitter(true);
    }
}
exports.DummyDiagnosticsEmitter = DummyDiagnosticsEmitter;
//# sourceMappingURL=DummyDiagnosticsEmitter.js.map