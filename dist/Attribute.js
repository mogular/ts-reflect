"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Attribute {
    constructor(id, parameters) {
        this.id = id;
        this.parameters = parameters;
    }
}
exports.Attribute = Attribute;
//# sourceMappingURL=Attribute.js.map