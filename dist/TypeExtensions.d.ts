import { Type } from 'ts-simple-ast';
export declare class TypeExtensions {
    static isPrimitive(type: Type): boolean;
}
