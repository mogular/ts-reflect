"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TypeExtensions {
    static isPrimitive(type) {
        let text = type.getText().toUpperCase();
        if (text == "STRING" || text == "NUMBER" || text == "BOOLEAN" || text == "OBJECT" || text == "ANY") {
            return true;
        }
        else {
            return false;
        }
    }
}
exports.TypeExtensions = TypeExtensions;
//# sourceMappingURL=TypeExtensions.js.map