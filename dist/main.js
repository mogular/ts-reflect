"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
const argparse_1 = require("argparse");
const TypeInfoResolver_1 = require("./TypeInfoResolver");
const fs = require("fs");
var Reflection_1 = require("./Reflection");
exports.Reflection = Reflection_1.Reflection;
var AttributeFactory_1 = require("./AttributeFactory");
exports.AttributeFactory = AttributeFactory_1.AttributeFactory;
var AttributeParser_1 = require("./AttributeParser");
exports.AttributeParser = AttributeParser_1.AttributeParser;
var Attribute_1 = require("./Attribute");
exports.Attribute = Attribute_1.Attribute;
var TypeInfo_1 = require("./typeinfos/TypeInfo");
exports.TypeInfo = TypeInfo_1.TypeInfo;
var ClassTypeInfo_1 = require("./typeinfos/ClassTypeInfo");
exports.ClassTypeInfo = ClassTypeInfo_1.ClassTypeInfo;
var InterfaceTypeInfo_1 = require("./typeinfos/InterfaceTypeInfo");
exports.InterfaceTypeInfo = InterfaceTypeInfo_1.InterfaceTypeInfo;
var IntersectionTypeInfo_1 = require("./typeinfos/IntersectionTypeInfo");
exports.IntersectionTypeInfo = IntersectionTypeInfo_1.IntersectionTypeInfo;
var AnonymousTypeInfo_1 = require("./typeinfos/AnonymousTypeInfo");
exports.AnonymousTypeInfo = AnonymousTypeInfo_1.AnonymousTypeInfo;
var ArrayTypeInfo_1 = require("./typeinfos/ArrayTypeInfo");
exports.ArrayTypeInfo = ArrayTypeInfo_1.ArrayTypeInfo;
var BooleanLiteralTypeInfo_1 = require("./typeinfos/BooleanLiteralTypeInfo");
exports.BooleanLiteralTypeInfo = BooleanLiteralTypeInfo_1.BooleanLiteralTypeInfo;
var EnumTypeInfo_1 = require("./typeinfos/EnumTypeInfo");
exports.EnumTypeInfo = EnumTypeInfo_1.EnumTypeInfo;
var NumberLiteralTypeInfo_1 = require("./typeinfos/NumberLiteralTypeInfo");
exports.NumberLiteralTypeInfo = NumberLiteralTypeInfo_1.NumberLiteralTypeInfo;
var StringLiteralTypeInfo_1 = require("./typeinfos/StringLiteralTypeInfo");
exports.StringLiteralTypeInfo = StringLiteralTypeInfo_1.StringLiteralTypeInfo;
var PrimitiveTypeInfo_1 = require("./typeinfos/PrimitiveTypeInfo");
exports.PrimitiveTypeInfo = PrimitiveTypeInfo_1.PrimitiveTypeInfo;
var UnionTypeInfo_1 = require("./typeinfos/UnionTypeInfo");
exports.UnionTypeInfo = UnionTypeInfo_1.UnionTypeInfo;
var PropertyTypeInfo_1 = require("./typeinfos/PropertyTypeInfo");
exports.PropertyTypeInfo = PropertyTypeInfo_1.PropertyTypeInfo;
var TypeInfoResolver_2 = require("./TypeInfoResolver");
exports.TypeInfoResolver = TypeInfoResolver_2.TypeInfoResolver;
// let path = "C:\\Users\\uli\\Documents\\bizboardhome\\bizboardweb\\src\\app\\interfaces\\Config.ts";
// Reflection.init();
// Reflection.addFile(path);
// let info = Reflection.getInterfaceTypeInfo("IFrontendConfig");
// console.log(JSON.stringify(info.resolver.dict, null, 3));
// let k = 10;
let parser = new argparse_1.ArgumentParser({
    addHelp: true,
    description: "generate typeinfos from .ts files"
});
parser.addArgument(["-f", "--file"], {
    help: ".ts files you want to add",
    required: true,
    nargs: "*"
});
parser.addArgument(["-c", "--class"], {
    help: "class types you want to extract",
    required: false,
    nargs: "*"
});
parser.addArgument(["-i", "--interface"], {
    help: "interface types you want to extract",
    required: false,
    nargs: "*"
});
parser.addArgument(["-o", "--out"], {
    help: "output file",
    required: true
});
let args = parser.parseArgs();
main_1.Reflection.init();
for (let file of args.file) {
    file = file.trim();
    console.log("Adding file " + file);
    main_1.Reflection.addFile(file);
}
let resolver = new TypeInfoResolver_1.TypeInfoResolver();
if (args.class) {
    for (let type of args.class) {
        type = type.trim();
        resolver = main_1.Reflection.getClassTypeInfo(type, resolver).resolver;
    }
}
if (args.interface) {
    for (let type of args.interface) {
        type = type.trim();
        resolver = main_1.Reflection.getInterfaceTypeInfo(type, resolver).resolver;
    }
}
let res = [];
for (let key in resolver.dict) {
    res.push(resolver.dict[key]);
}
let text = JSON.stringify(res);
console.log("Writing output to " + args.out);
fs.writeFileSync(args.out, text);
// else
// {
//     throw new Error("No path specified");
// }
//# sourceMappingURL=main.js.map