import Project, { SourceFile, ClassDeclaration, ClassInstancePropertyTypes, Type, ExportDeclaration, SyntaxKind, ExpressionWithTypeArguments, ts } from "ts-simple-ast";
import { AttributeFactory } from './AttributeFactory';
import { AttributeParser } from './AttributeParser';
import { TypeInfo } from './typeinfos/TypeInfo';

import { InterfaceDeclaration } from 'ts-simple-ast';
import * as path from "path";
import { ClassTypeInfo } from "./typeinfos/ClassTypeInfo";
import { InterfaceTypeInfo } from './typeinfos/InterfaceTypeInfo';
import { TypeInfoResolver } from './TypeInfoResolver';

export class Reflection
{
    public static project: Project;

    public static init()
    {
        this.project = new Project();
    }

    public static createFile(path: string, text: string): SourceFile
    {
        try
        {
            return this.project.createSourceFile(path, text);
        }
        catch(error)
        {
            console.log(error);
            return null;
        }
    }

    public static clear()
    {
        Reflection.project = null;
    }

    public static addFile(path: string): SourceFile
    {
        try
        {
            return this.project.addExistingSourceFile(path);
        }
        catch(error)
        {
            console.log(error);
            return null;
        }
    }

    public static addDir(dirPath: string)
    {
        try
        {
            this.project.addExistingSourceFiles(path.join(dirPath, "*.ts"));
        }
        catch(error)
        {
            console.log(error);
        }
    }

    public static searchType(name: string)
    {
        let upper = name.toUpperCase();
        let c: Type = null;
        for(let file of this.project.getSourceFiles())
        {
            if(upper == "ANY")
                return this.project.getTypeChecker().compilerObject.typeParameterToDeclaration
            c = file.getClass(name).getType();
            if(c)
                break;
            c = file.getInterface(name).getType();
            if(c)
                break;
        }
        return c;
    }

    public static getClassTypeInfo(name: string, resolver: TypeInfoResolver = null): {info: TypeInfo, resolver: TypeInfoResolver}
    {
        let c: ClassDeclaration = null;
        for(let file of this.project.getSourceFiles())
        {
            c = file.getClass(name);
            if(c)
            break;
        }
        
        if(!c)
        return null;

        if(!resolver)
            resolver = new TypeInfoResolver();
        let n = ClassTypeInfo.fromClassDeclaration(c, resolver);
        return {info: resolver.resolve(n), resolver: resolver};
    }

    public static getInterfaceTypeInfo(name: string, resolver: TypeInfoResolver): {info: TypeInfo, resolver: TypeInfoResolver}
    {
        let c: InterfaceDeclaration = null;
        for(let file of this.project.getSourceFiles())
        {
            c = file.getInterface(name);
            if(c)
                break;
        }

        if(!c)
            return null;
        
                    
        if(!resolver)
            resolver = new TypeInfoResolver();
        let n = InterfaceTypeInfo.fromInterfaceDeclaration(c, resolver);
        return {info: resolver.resolve(n), resolver: resolver};
    }

    public static getDescendantDeclarations(name: string): (InterfaceDeclaration | ClassDeclaration)[]
    {
        let res: (InterfaceDeclaration|ClassDeclaration)[] = [];
        for(let file of this.project.getSourceFiles().filter(f => f.getExtension() == ".ts"))
        {
            try
            {
                let matchingDeclarations = file.getExportedDeclarations()
                    .filter(d => d.getKind() == SyntaxKind.InterfaceDeclaration || d.getKind() == SyntaxKind.ClassDeclaration)
                    .filter(d => {
                        let declaration = d as InterfaceDeclaration | ClassDeclaration;
                        let ex = declaration.getExtends();
                        if(ex)
                        {
                            if(Array.isArray(ex))
                            {
                                return (ex as ExpressionWithTypeArguments[]).map(e => e.getType().getSymbol().getName()).indexOf(name) >= 0;
                            }
                            else
                            {
                                return (ex as ExpressionWithTypeArguments).getType().getSymbol().getName() == name;
                            }
                        }
                    }) as (InterfaceDeclaration | ClassDeclaration)[];
                res = res.concat(matchingDeclarations);
            }
            catch(error)
            {
                console.log(`Could not get descendant types, error: ${error}`);
                return null;
            }
        }
        return res;
    }
}



