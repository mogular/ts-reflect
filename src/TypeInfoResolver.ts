import { TypeInfo } from './typeinfos/TypeInfo';
import { TypeParameterTypeInfo } from './typeinfos/TypeParameterTypeInfo';
export class TypeInfoResolver
{
    public static TYPE_PARAMETER_NAME = "_TYPE_PARAMETER";
    public dict: any = {};

    constructor()
    {
        this.register(TypeInfoResolver.TYPE_PARAMETER_NAME, new TypeParameterTypeInfo())
    }
    
    public register(name: string, info: TypeInfo)
    {
        if(name == null || name != info.name)
            throw new Error("TypeInfo name must be set and info.name must match name!");
        this.dict[name] = info;
    }

    public resolve(name: string): TypeInfo
    {
        return this.dict[name];
    }

    public hasInfo(name: string): boolean
    {
        if(!name)
            return false;
        return this.dict[name] != null;
    }
}