import { Node, AttributeParser } from './AttributeParser';
import { Constructor, JSDoc } from 'ts-simple-ast';
import { Attribute } from './Attribute';
export class AttributeFactory
{
    // public static attributeList: {id: string, ctor: Constructor<Attribute>}[] = [];

    // public static registerAttribute(attribute: Attribute)
    // {
    //     this.attributeList.push({id: attribute.id, ctor: (<any> attribute).constructor});
    // }

    public static createAttributes(node: Node): Attribute[]
    {
        let res: Attribute[] = [];
        for(let attributeDeclarationNode of node.children)
        {
            for(let attributeNode of attributeDeclarationNode.children)
            {
                let params = attributeNode.children.map(c => c.value);
                res.push(new Attribute(attributeNode.value, params));
            }
        }
        return res;
    }

    public static getAttributesFromJsDocs(jsdocs: JSDoc[]): Attribute[]
    {
        if(jsdocs && jsdocs.length > 0)
        {
            let doc = jsdocs.map(c => c.getComment()).join(" ");
            let parser = new AttributeParser();
            let node = parser.parse(doc);
            return AttributeFactory.createAttributes(node);
        }
        return [];
    }
}
