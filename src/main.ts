import { Reflection } from "./main";
import { ArgumentParser } from "argparse";
import { TypeInfoResolver } from "./TypeInfoResolver";
import * as fs from "fs";

export { Reflection } from "./Reflection";
export { AttributeFactory } from "./AttributeFactory";
export { AttributeParser } from "./AttributeParser";
export { Attribute } from "./Attribute";
export { TypeInfo } from "./typeinfos/TypeInfo";
export { ClassTypeInfo } from "./typeinfos/ClassTypeInfo";
export { InterfaceTypeInfo } from "./typeinfos/InterfaceTypeInfo";
export { IntersectionTypeInfo } from "./typeinfos/IntersectionTypeInfo";
export { AnonymousTypeInfo } from "./typeinfos/AnonymousTypeInfo";
export { ArrayTypeInfo } from "./typeinfos/ArrayTypeInfo";
export { BooleanLiteralTypeInfo } from "./typeinfos/BooleanLiteralTypeInfo";
export { EnumTypeInfo } from "./typeinfos/EnumTypeInfo";
export { NumberLiteralTypeInfo } from "./typeinfos/NumberLiteralTypeInfo";
export { StringLiteralTypeInfo } from "./typeinfos/StringLiteralTypeInfo";
export { PrimitiveTypeInfo } from "./typeinfos/PrimitiveTypeInfo";
export { UnionTypeInfo } from "./typeinfos/UnionTypeInfo";
export { PropertyTypeInfo } from "./typeinfos/PropertyTypeInfo";
export { TypeInfoResolver } from "./TypeInfoResolver";

// let path = "C:\\Users\\uli\\Documents\\bizboardhome\\bizboardweb\\src\\app\\interfaces\\Config.ts";
// Reflection.init();
// Reflection.addFile(path);
// let info = Reflection.getInterfaceTypeInfo("IFrontendConfig");
// console.log(JSON.stringify(info.resolver.dict, null, 3));
// let k = 10;

let parser = new ArgumentParser({
    addHelp: true,
    description: "generate typeinfos from .ts files"
});

parser.addArgument(["-f", "--file"], {
    help: ".ts files you want to add",
    required: true,
    nargs: "*"
});
parser.addArgument(["-c", "--class"], {
    help: "class types you want to extract",
    required: false,
    nargs: "*"
});
parser.addArgument(["-i", "--interface"], {
    help: "interface types you want to extract",
    required: false,
    nargs: "*"
});
parser.addArgument(["-o", "--out"], {
    help: "output file",
    required: true
});
let args = parser.parseArgs();
Reflection.init();
for(let file of args.file)
{
    file = file.trim();
    console.log("Adding file "+file)
    Reflection.addFile(file);
}
let resolver = new TypeInfoResolver();
if(args.class)
{
    for(let type of args.class)
    {
        type = type.trim();
        resolver = Reflection.getClassTypeInfo(type, resolver).resolver;
    }
}
if(args.interface)
{
    for(let type of args.interface)
    {
        type = type.trim();
        resolver = Reflection.getInterfaceTypeInfo(type, resolver).resolver;
    }
}
let res = [];
for(let key in resolver.dict)
{
    res.push(resolver.dict[key]);
}
let text = JSON.stringify(res);
console.log("Writing output to "+args.out);
fs.writeFileSync(args.out, text);




// else
// {
//     throw new Error("No path specified");
// }

