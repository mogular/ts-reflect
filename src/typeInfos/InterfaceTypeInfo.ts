import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { Type, InterfaceDeclaration } from "ts-simple-ast";
import { AttributeParser } from '../AttributeParser';
import { AttributeFactory } from '../AttributeFactory';
import { TypeInfoResolver } from '../TypeInfoResolver';

export class InterfaceTypeInfo extends TypeInfo
{
    public properties: PropertyTypeInfo[] = [];
    public attributes: Attribute[] = [];
    public text: string;
    public extends: string[] = [];

    constructor()
    {
        super(TypeInfoKind.Interface);
    }

    public static fromInterfaceDeclaration(id: InterfaceDeclaration, resolver: TypeInfoResolver): string
    {
        let name = id.getName();
        if(resolver.hasInfo(name) == true)
            return name;
        
        let info = new InterfaceTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.text = id.getText();

        info.attributes = AttributeFactory.getAttributesFromJsDocs(id.getJsDocs());

        let properties = id.getProperties();
        for(let prop of properties)
        {
            info.properties.push(PropertyTypeInfo.fromPropertySignature(prop, resolver));
        }

        let extend = id.getExtends();
        if(extend)
        {
            for(let e of extend)
            {
                info.extends.push(InterfaceTypeInfo.fromType(e.getType(), resolver));
            }
        }
        return name;
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let id = type.getSymbol().getDeclarations()[0] as InterfaceDeclaration;
        return InterfaceTypeInfo.fromInterfaceDeclaration(id, resolver);
    }
}