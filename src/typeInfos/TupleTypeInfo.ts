import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from "../main";
import { Factory } from "./Factory";

export class TupleTypeInfo extends TypeInfo
{
    public value: string;
    public static cnt = 0;
    public tupleTypes: string[] = [];

    constructor()
    {
        super(TypeInfoKind.Tuple);
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let name: string;
        let tupleTypes = type.getTupleElements();
        let symbol = type.getSymbol();
        let tupleNames = tupleTypes.map(t => Factory.fromType(t, resolver));
        if(symbol)
            name = symbol.getName();
        else
            name = "["+ tupleNames.join(",") + "]";

        if(resolver.hasInfo(name) == true)
            return name;

        let info = new TupleTypeInfo();
        info.name = name;
        info.tupleTypes = tupleNames;
        resolver.register(name, info);
        return info.name;
    }
}