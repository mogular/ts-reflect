import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { Type } from 'ts-simple-ast';
import { Factory } from './Factory';
import { TypeInfoResolver } from '../TypeInfoResolver';

export class UnionTypeInfo extends TypeInfo
{
    public unionTypes: string[] = [];
    
    constructor()
    {
        super(TypeInfoKind.Union);
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let name: string;
        let unTypes = type.getUnionTypes();
        let symbol = type.getSymbol();
        if(symbol)
            name = symbol.getName();
        else
            name = unTypes.map(t => Factory.fromType(t, resolver)).join(" | ");

        if(resolver.hasInfo(name) == true)
            return name;

        let info = new UnionTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.unionTypes = unTypes.map(u => Factory.fromType(u, resolver));
        return name;
    }
}