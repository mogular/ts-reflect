import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { Type, JSDoc, TypeParameter } from 'ts-simple-ast';
import { TypeInfoResolver } from '../TypeInfoResolver';
import { AttributeFactory } from "../main";
import { Factory } from "./Factory";
import { Reflection } from '../Reflection';


export class GenericTypeInfo extends TypeInfo
{
    public attributes: Attribute[] = [];
    public text: string;
    public targetType: string;
    public typeArguments: string[] = [];

    constructor()
    {
        super(TypeInfoKind.Generic);
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let symbol = type.getSymbol();
        let args = type.getTypeArguments().map(a => a.getText());
        let name = symbol.getName() + `<${args.join(",")}>`;
        if(resolver.hasInfo(name) == true)
            return name;
        
        let info = new GenericTypeInfo();
        info.name = name;
        info.typeArguments = type.getTypeArguments().map(t => {
            let text = t.getText();
            let upper = text.toUpperCase();
            if(upper == "ANY" || upper == "STRING" || upper == "NUMBER" || upper == "BOOLEAN" || upper == "OBJECT") //This is pretty much dirty but it works...
                return upper;
            return text;
        });
        resolver.register(name, info);
        info.targetType = Factory.fromType(type.getTargetType(), resolver);
        info.text = type.getText();
        info.attributes = AttributeFactory.getAttributesFromJsDocs((type.compilerType as any).jsDoc);
        return info.name;
    }

}