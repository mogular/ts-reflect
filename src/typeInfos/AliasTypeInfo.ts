import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';


export class AliasTypeInfo extends TypeInfo
{
    public typeName: string;

    constructor(aliasName: string, typeName: string)
    {
        super(TypeInfoKind.Alias);
        this.name = aliasName;
        this.typeName = typeName;
    }
}