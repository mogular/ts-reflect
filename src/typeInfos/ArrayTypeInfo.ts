import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { Type } from 'ts-simple-ast';
import { Factory } from './Factory';
import { TypeInfoResolver } from "../TypeInfoResolver";

export class ArrayTypeInfo extends TypeInfo
{
    public arrayType: string;

    constructor()
    {
        super(TypeInfoKind.Array);
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let arType = type.getArrayType();
        let arTypeName = Factory.fromType(arType, resolver);
        let name = arTypeName + "[]";
        if(resolver.hasInfo(name) == true)
            return name;
        
        let info = new ArrayTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.arrayType = arTypeName;
        return name;
    }
}