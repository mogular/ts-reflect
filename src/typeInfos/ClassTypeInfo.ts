import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { ClassDeclaration, Type } from "ts-simple-ast";
import { AttributeFactory } from '../AttributeFactory';
import { AttributeParser } from "../AttributeParser";
import { InterfaceTypeInfo } from "./InterfaceTypeInfo";
import { TypeInfoResolver } from "../TypeInfoResolver";

export class ClassTypeInfo extends TypeInfo
{
    public properties: PropertyTypeInfo[] = [];
    public attributes: Attribute[] = [];
    public text: string;
    public extends: string[] = [];
    public implements: string[] = [];

    constructor()
    {
        super(TypeInfoKind.Class);
    }

    public static fromClassDeclaration(cd: ClassDeclaration, resolver: TypeInfoResolver): string
    {
        let name = cd.getName();
        if(resolver.hasInfo(name) == true)
            return name;
        
        let info = new ClassTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.text = cd.getText();

        info.attributes = AttributeFactory.getAttributesFromJsDocs(cd.getJsDocs());

        let properties = cd.getInstanceProperties();
        for(let prop of properties)
        {
            info.properties.push(PropertyTypeInfo.fromClassInstancePropertyTypes(prop, resolver));
        }

        let extend = cd.getExtends();
        if(extend)
        {
            let type = extend.getType();
            let eInfo = ClassTypeInfo.fromType(type, resolver);
        }
        let impl = cd.getImplements();
        if(impl)
        {
            for(let i of impl)
            {
                info.implements.push(InterfaceTypeInfo.fromType(i.getType(), resolver));
            }
        }
        return info.name;
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let cd = type.getSymbol().getValueDeclaration() as ClassDeclaration;
        return ClassTypeInfo.fromClassDeclaration(cd, resolver);
    }
}