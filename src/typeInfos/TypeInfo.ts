export enum TypeInfoKind
{
    Primitive = 0, 
    Interface, 
    Class, 
    Enum, 
    Union, 
    Intersection, 
    Array, 
    StringLiteral, 
    NumberLiteral, 
    BooleanLiteral, 
    Anonymous,
    Tuple,
    Alias,
    Generic,
    TypeParameter
}

export class TypeInfo
{
    public name: string;
    constructor(private kind: TypeInfoKind) 
    {
        
    }
}