import { TypeInfo, TypeInfoKind } from './TypeInfo';
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';


export class TypeParameterTypeInfo extends TypeInfo
{
    constructor()
    {
        super(TypeInfoKind.TypeParameter);
        this.name = TypeInfoResolver.TYPE_PARAMETER_NAME;
    }
}