import { TypeInfo, TypeInfoKind } from "./TypeInfo"

export class PrimitiveTypeInfo extends TypeInfo
{
    constructor() 
    {
        super(TypeInfoKind.Primitive);
    }
}