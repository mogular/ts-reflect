import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { EnumDeclaration, Type } from 'ts-simple-ast';
import { AttributeParser } from '../AttributeParser';
import { AttributeFactory } from '../AttributeFactory';
import { TypeInfoResolver } from '../TypeInfoResolver';

export interface EnumMember
{
    value: number|string;
    key: string;
}

export class EnumTypeInfo extends TypeInfo
{
    public members: EnumMember[] = [];
    public attributes: Attribute[] = [];
    
    constructor()
    {
        super(TypeInfoKind.Enum);
    }

    public static fromDeclaration(ed: EnumDeclaration, resolver: TypeInfoResolver): string
    {
        let name = ed.getName();
        if(resolver.hasInfo(name) == true)
            return name;

        let info = new EnumTypeInfo();
        info.name = name;
        resolver.register(name, info);
        info.members = ed.getMembers().map(m => {
            return {key: m.getName(), value: m.getValue()};
        });

        info.attributes = AttributeFactory.getAttributesFromJsDocs(ed.getJsDocs());
        return name;
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        return EnumTypeInfo.fromDeclaration(type.getSymbol().getValueDeclaration() as EnumDeclaration, resolver);
    }
}