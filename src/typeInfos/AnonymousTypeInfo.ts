import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { PropertyTypeInfo } from './PropertyTypeInfo';
import { Attribute } from '../Attribute';
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';


export class AnonymousTypeInfo extends TypeInfo
{
    public static cnt = 0;
    public properties: PropertyTypeInfo[] = [];

    constructor()
    {
        super(TypeInfoKind.Anonymous);
        this.name = "Anonymous-"+AnonymousTypeInfo.cnt;
        AnonymousTypeInfo.cnt++;
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let info = new AnonymousTypeInfo();
        let properties = type.getProperties();
        for(let prop of properties)
        {
            info.properties.push(PropertyTypeInfo.fromSymbol(prop, resolver));
        }
        resolver.register(info.name, info);
        return info.name;
    }
}