enum AttributeParserState { Idle, ReadingAttributes, ReadingAttributeId, ReadingAttributeParameters, 
                            ReadingStringAttributeParameter, ReadingNumberAttributeParameter,
                            ReadingEscapeCharacter }

export enum NodeKind
{
    Root = 0, AttributeDeclaration, Attribute, StringParameter, NumberParameter
}

export class AttributeParser
{
    private state: AttributeParserState = AttributeParserState.Idle;
    private buffer: string = "";
    private whitespaceRegex = /\s/;
    private symbolRegex = /[\w_]/;
    private numberRegex = /[0-9]/;
    private position = 0;
    private rootNode = new Node(NodeKind.Root);
    private currentNode: Node = null;

    public parse(text: string)
    {
        let state = AttributeParserState.Idle;
        for(let i = 0; i<text.length; i++)
        {
            this.processToken(text[i]);
            this.position++;
        }
        let ret = this.rootNode;
        this.rootNode = new Node(NodeKind.Root);
        this.currentNode = this.rootNode;
        this.position = 0;
        return ret;
    }

    private processToken(token: string)
    {
        switch(this.state)
        {
            case AttributeParserState.Idle:
            {
                if(token == "[")
                {
                    this.currentNode = this.rootNode.addChild(NodeKind.AttributeDeclaration);
                    this.currentNode.start = this.position;
                    this.state = AttributeParserState.ReadingAttributes;
                }
                break;
            }
            case AttributeParserState.ReadingAttributes:
            {
                if(token.match(this.symbolRegex))
                {
                    this.buffer += token;
                    let node = this.currentNode.addChild(NodeKind.Attribute);
                    node.start = this.position;
                    this.currentNode = node;
                    this.state = AttributeParserState.ReadingAttributeId;
                }
                else if(token == "]")
                {
                    this.currentNode.end = this.position;
                    this.state = AttributeParserState.Idle;
                    this.currentNode = this.rootNode;
                }
                else if(token == ",")
                {

                }
                else if(token.match(this.whitespaceRegex))
                {

                }
                else
                {
                    console.log(`Unexpected token ${token} at position ${this.position}`);
                }
                break;
            }
            case AttributeParserState.ReadingAttributeId:
            {
                if(token.match(this.symbolRegex))
                {
                    this.buffer += token;
                }
                else if(token == "(")
                {
                    this.currentNode.value = this.buffer;
                    this.buffer = "";
                    this.currentNode.end = this.position;
                    this.state = AttributeParserState.ReadingAttributeParameters;
                }
                else if(token == ",")
                {
                    this.currentNode.value = this.buffer;
                    this.buffer = "";
                    this.currentNode.end = this.position;
                    this.currentNode = this.currentNode.parent; //set current node to NodeKind.AttributeDeclaration
                    this.state = AttributeParserState.ReadingAttributes;
                }
                else if(token.match(this.whitespaceRegex))
                {
                    this.currentNode.value = this.buffer;
                    this.buffer = "";
                    this.currentNode.end = this.position;
                    this.currentNode = this.currentNode.parent;
                    this.state = AttributeParserState.ReadingAttributes;
                }
                else if(token == "]")
                {
                    this.currentNode.value = this.buffer;
                    this.buffer = "";
                    this.currentNode.end = this.position;
                    this.state = AttributeParserState.Idle;
                    this.currentNode = this.rootNode;
                }
                break;
            }
            case AttributeParserState.ReadingAttributeParameters:
            {
                if(token.match(this.numberRegex))
                {
                    this.buffer += token;
                    let node = this.currentNode.addChild(NodeKind.NumberParameter);
                    node.start = this.position;
                    this.currentNode = node;
                    this.state = AttributeParserState.ReadingNumberAttributeParameter;
                }
                if(token == "\"")
                {
                    let node = this.currentNode.addChild(NodeKind.StringParameter);
                    node.start = this.position;
                    this.currentNode = node;
                    this.state = AttributeParserState.ReadingStringAttributeParameter;
                }
                else if(token == ")")
                {
                    this.currentNode.end = this.position;
                    this.currentNode = this.currentNode.parent;
                    this.state = AttributeParserState.ReadingAttributes;
                }
                else if(token == ",")
                {

                }
                else if(token.match(this.whitespaceRegex))
                {

                }
                else
                {
                    console.log(`Unexpected token ${token} at position ${this.position}`);
                }
                break;
            }
            case AttributeParserState.ReadingNumberAttributeParameter:
            {
                if(token.match(this.numberRegex))
                {
                    this.buffer += token;
                }
                else if(token == ",")
                {
                    this.currentNode.value = this.buffer;
                    this.buffer = "";
                    this.currentNode.end = this.position;
                    this.currentNode = this.currentNode.parent;
                    this.state = AttributeParserState.ReadingAttributeParameters;
                }
                else if(token.match(this.whitespaceRegex))
                {
                    this.currentNode.value = this.buffer;
                    this.buffer = "";
                    this.currentNode.end = this.position;
                    this.currentNode = this.currentNode.parent;
                    this.state = AttributeParserState.ReadingAttributeParameters;
                }
                break;
            }
            case AttributeParserState.ReadingStringAttributeParameter:
            {
                if(token == "\\")
                {
                    this.state = AttributeParserState.ReadingEscapeCharacter;
                }
                else if(token == "\"")
                {
                    this.currentNode.value = this.buffer;
                    this.buffer = "";
                    this.currentNode.end = this.position;
                    this.currentNode = this.currentNode.parent;
                    this.state = AttributeParserState.ReadingAttributeParameters;
                }
                else 
                {
                    this.buffer += token;
                }
                break;
            }
            case AttributeParserState.ReadingEscapeCharacter:
            {
                if(token == "a")
                {
                    this.buffer += "\a";
                }
                else if(token == "b")
                {
                    this.buffer += "\b";
                }
                else if(token == "e")
                {
                    this.buffer += "\e";
                }
                else if(token == "E")
                {
                    this.buffer += "\E";
                }
                else if(token == "f")
                {
                    this.buffer += "\f";
                }
                else if(token == "n")
                {
                    this.buffer += "\n";
                }
                else if(token == "t")
                {
                    this.buffer += "\t";
                }
                else if(token == "v")
                {
                    this.buffer += "\v";
                }
                else if(token == "\"")
                {
                    this.buffer += "\"";
                }
                else
                {
                    console.log(`Unrecognized escape sequence \\${token}`)
                }
                this.state = AttributeParserState.ReadingStringAttributeParameter;
                break;
            }
        }
    }
}

export class Node
{
    public value: string;
    public start: number = null;
    public end: number = null;
    public children: Node[] = [];
    public parent: Node;
    public kindString: string;

    constructor(public kind: NodeKind, parent: Node = null, value: string = null, start: number = null, end: number = null)
    {
        this.parent = parent;
        this.value = value;
        this.start = start;
        this.end = end;
        this.kindString = NodeKind[kind];
    }

    public addChild(kind: NodeKind, value: string = null, start: number = null, end: number = null): Node
    {
        let node = new Node(kind, this);
        this.children.push(node);
        return node;
    }
}