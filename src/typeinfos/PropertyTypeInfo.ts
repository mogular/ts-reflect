import { TypeInfo } from "./TypeInfo";
import { Attribute } from '../Attribute';
import { ClassInstancePropertyTypes, PropertySignature, Symbol } from 'ts-simple-ast';
import { AttributeParser } from "../AttributeParser";
import { AttributeFactory } from '../AttributeFactory';
import { Factory } from './Factory';
import { TypeInfoResolver } from "../TypeInfoResolver";

export class PropertyTypeInfo
{
    public name: string;
    public type: string;
    public attributes: Attribute[] = [];
    public isRequired: boolean = true;

    public static fromClassInstancePropertyTypes(p: ClassInstancePropertyTypes, resolver: TypeInfoResolver): PropertyTypeInfo
    {
        let prop = new PropertyTypeInfo();
        prop.name = p.getName();
        if(p.compilerNode.questionToken)
        {
            prop.isRequired = false;
        }

        prop.attributes = AttributeFactory.getAttributesFromJsDocs((p.compilerNode as any).jsDoc);
        prop.type = Factory.fromType(p.getType(), resolver);
        return prop;
    }

    public static fromPropertySignature(p: PropertySignature, resolver: TypeInfoResolver): PropertyTypeInfo
    {
        let prop = new PropertyTypeInfo();
        prop.name = p.getName();
        if(p.compilerNode.questionToken)
        {
            prop.isRequired = false;
        }

        prop.attributes = AttributeFactory.getAttributesFromJsDocs(p.getJsDocs());
        prop.type = Factory.fromType(p.getType(), resolver);
        return prop;
    }

    public static fromSymbol(s: Symbol, resolver: TypeInfoResolver): PropertyTypeInfo
    {
        return PropertyTypeInfo.fromPropertySignature(s.getValueDeclaration() as PropertySignature, resolver);
    }
}