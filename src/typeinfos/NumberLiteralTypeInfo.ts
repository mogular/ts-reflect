import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';

export class NumberLiteralTypeInfo extends TypeInfo
{
    public value: number;
    public static cnt = 0;
    constructor()
    {
        super(TypeInfoKind.NumberLiteral);
        this.name = "NumberLiteral-" + NumberLiteralTypeInfo.cnt;
        NumberLiteralTypeInfo.cnt++;
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let info = new NumberLiteralTypeInfo();
        info.value = Number.parseFloat(type.getText().trim());
        resolver.register(info.name, info);
        return info.name;
    }
}