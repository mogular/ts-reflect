import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { Type } from "ts-simple-ast";
import { TypeInfoResolver } from '../TypeInfoResolver';

export class BooleanLiteralTypeInfo extends TypeInfo
{
    public value: boolean;
    public static cnt = 0;
    constructor()
    {
        super(TypeInfoKind.BooleanLiteral);
        this.name = "BooleanLiteral-" + BooleanLiteralTypeInfo.cnt;
        BooleanLiteralTypeInfo.cnt++;
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let info = new BooleanLiteralTypeInfo();
        info.value = type.getText() == "true";
        resolver.register(info.name, info);
        return info.name;
    }
}