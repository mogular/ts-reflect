import { Type, ClassDeclaration, StringLiteral } from 'ts-simple-ast';
import { ts } from "ts-simple-ast"
import { PrimitiveTypeInfo } from './PrimitiveTypeInfo';
import { ClassTypeInfo } from './ClassTypeInfo';
import { InterfaceTypeInfo } from './InterfaceTypeInfo';
import { AnonymousTypeInfo } from './AnonymousTypeInfo';
import { ArrayTypeInfo } from './ArrayTypeInfo';
import { EnumTypeInfo } from './EnumTypeInfo';
import { UnionTypeInfo } from './UnionTypeInfo';
import { IntersectionTypeInfo } from './IntersectionTypeInfo';
import { NumberLiteralTypeInfo } from './NumberLiteralTypeInfo';
import { BooleanLiteralTypeInfo } from './BooleanLiteralTypeInfo';
import { StringLiteralTypeInfo } from './StringLiteralTypeInfo';
import { TypeInfo } from './TypeInfo';
import { TypeInfoResolver } from '../TypeInfoResolver';
import { TupleTypeInfo } from './TupleTypeInfo';
import { AliasTypeInfo } from './AliasTypeInfo';
import { GenericTypeInfo } from './GenericTypeInfo';
import { TypeParameterTypeInfo } from './TypeParameterTypeInfo';

export class Factory
{
    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let symbol = type.getSymbol();
        let name: string;
        if(symbol)
        {
            name = symbol.getName();
            if(resolver.hasInfo(name) == true)
                return name;
        }

        let text = type.getText();
        let upperText = text.toUpperCase();

        if(type.isString() === true || type.isNumber() === true || type.isBoolean() === true || type.isUndefined() === true)
        {
            name = upperText;
            if(resolver.hasInfo(name) == true)
                return name;

            let info = new PrimitiveTypeInfo();
            info.name = upperText;
            resolver.register(info.name, info);
            name = info.name;
        }
        else if(upperText == "ANY" || upperText == "OBJECT")
        {
            name = upperText;
            if(resolver.hasInfo(name) == true)
                return name;

            let info = new PrimitiveTypeInfo();
            info.name = upperText;
            resolver.register(info.name, info);
            name = info.name;
        }
        else if(type.isArray() === true)
        {
            name = ArrayTypeInfo.fromType(type, resolver);
        }
        else if(type.isTuple() === true)
        {
            name = TupleTypeInfo.fromType(type, resolver);
        }
        else if(type.isEnum() === true || type.isEnumLiteral())
        {
            name = EnumTypeInfo.fromType(type, resolver);
        }
        else if(type.isUnion() === true)
        {
            name = UnionTypeInfo.fromType(type, resolver);
        }
        else if(type.isIntersection() === true)
        {
            name = IntersectionTypeInfo.fromType(type, resolver);
        }
        else if(type.isAnonymous() === true)
        {
            name = AnonymousTypeInfo.fromType(type, resolver);   
        }
        else if(type.isClass() === true)
        {
            name = ClassTypeInfo.fromType(type, resolver);
        }
        else if(type.isInterface() === true)
        {
            name = InterfaceTypeInfo.fromType(type, resolver);
        }
        else if(type.isNumberLiteral() === true)
        {
            name = NumberLiteralTypeInfo.fromType(type, resolver);
        }
        else if(type.isBooleanLiteral() === true)
        {
            name = BooleanLiteralTypeInfo.fromType(type, resolver);
        }
        else if(type.isStringLiteral() === true)
        {
            name = StringLiteralTypeInfo.fromType(type, resolver);
        }
        else if(type.getTypeArguments().length > 0)
        {
            name = GenericTypeInfo.fromType(type, resolver);
        }
        else if(type.isTypeParameter())
        {
            name = resolver.resolve(TypeInfoResolver.TYPE_PARAMETER_NAME).name;
        }
        else
        {
            console.log(`Warning: No factory case applies to type ${name}, returning 'any' AliasType!`);
            let t = new AliasTypeInfo(name, "ANY");
            resolver.register(name, t);
        }
        return name;
    }

}