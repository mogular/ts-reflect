import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { Type } from 'ts-simple-ast';
import { Factory } from './Factory';
import { TypeInfoResolver } from '../TypeInfoResolver';

export class IntersectionTypeInfo extends TypeInfo
{
    public intersectionTypes: string[] = [];
    
    constructor()
    {
        super(TypeInfoKind.Interface);
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let intTypes = type.getIntersectionTypes();
        let symbol = type.getSymbol();
        let name: string;
        if(symbol)
            name = symbol.getName();
        else 
            name = intTypes.map(t => Factory.fromType(t, resolver)).join(" | ");
        
        if(resolver.hasInfo(name) == true)
            return name;

        let info = new IntersectionTypeInfo;
        info.name = name;
        resolver.register(name, info);
        info.intersectionTypes = type.getIntersectionTypes().map(t => {
            return Factory.fromType(t, resolver);
        });
        return name;
    }
}