import { TypeInfo, TypeInfoKind } from "./TypeInfo";
import { Type } from 'ts-simple-ast';
import { TypeInfoResolver } from '../TypeInfoResolver';

export class StringLiteralTypeInfo extends TypeInfo
{
    public value: string;
    public static cnt = 0;
    constructor()
    {
        super(TypeInfoKind.StringLiteral);
        this.name = "StringLiteral-"+StringLiteralTypeInfo.cnt;
        StringLiteralTypeInfo.cnt++;
    }

    public static fromType(type: Type, resolver: TypeInfoResolver): string
    {
        let info = new StringLiteralTypeInfo();
        info.value = type.getText();
        if(info.value.startsWith("\"") || info.value.startsWith("'"))
            info.value = info.value.substr(1);
        if(info.value.endsWith("\"") || info.value.endsWith("'"))
            info.value = info.value.substr(0, info.value.length - 1);
        resolver.register(info.name, info);
        return info.name;
    }
}